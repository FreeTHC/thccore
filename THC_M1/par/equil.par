ActiveThorns = "
ADMBase
ADMCoupling
ADMMacros
AEILocalInterp
Boundary
Carpet
CarpetIOASCII
CarpetIOBasic
CarpetIOScalar
CarpetInterp
CarpetLib
CarpetReduce
CarpetSlab
CartGrid3d
CoordBase
CoordGauge
GenericFD
HydroBase
HRSCCore
EOS_Thermal
EOS_Thermal_Extable
EOS_Thermal_Table3d
InitBase
IOUtil
LoopControl
MoL
Slab
StaticConformal
SymBase
THC_Core
THC_InitialData
THC_M1
THC_Tracer
Time
TmunuBase
WeakRates
"

Cactus::terminate			= "iteration"
Cactus::cctk_final_time		= 100.0
Cactus::cctk_itlast			= 10000

# =============================================================================
# Grid
# =============================================================================
Grid::avoid_origin			= "no"
Grid::domain			= "full"
Grid::type			= "coordbase"

CoordBase::xmin			= -1
CoordBase::xmax			= 1
CoordBase::ymin			= -1
CoordBase::ymax			= 1
CoordBase::zmin			= -1
CoordBase::zmax			= 1

CoordBase::spacing			= "numcells"
CoordBase::ncells_x			= 1
CoordBase::ncells_y			= 1
CoordBase::ncells_z			= 1

CoordBase::boundary_size_x_lower		= 2
CoordBase::boundary_size_x_upper		= 2
CoordBase::boundary_shiftout_x_lower		= 0
CoordBase::boundary_shiftout_x_upper		= 1

CoordBase::boundary_size_y_lower		= 2
CoordBase::boundary_size_y_upper		= 2
CoordBase::boundary_shiftout_y_lower		= 0
CoordBase::boundary_shiftout_y_upper		= 1

CoordBase::boundary_size_z_lower		= 2
CoordBase::boundary_size_z_upper		= 2
CoordBase::boundary_shiftout_z_lower		= 0
CoordBase::boundary_shiftout_z_upper		= 1

Driver::ghost_size			= 2
Driver::ghost_size_x		= 2
Driver::ghost_size_y		= 2
Driver::ghost_size_z		= 2

Carpet::domain_from_coordbase		= "yes"

InitBase::initial_data_setup_method		= "init_all_levels"

Carpet::max_refinement_levels		= 1
Carpet::prolongation_order_space		= 3
Carpet::prolongation_order_time		= 2
Carpet::use_buffer_zones		= "yes"
Carpet::enable_all_storage		= "no"

Carpet::grid_coordinates_filename		= "grid.carpet"

# =============================================================================
# Initial data
# =============================================================================
HydroBase::initial_hydro		= "THC_Initial"
HydroBase::initial_Y_e		= "THC_Initial"
HydroBase::initial_temperature		= "zero"
HydroBase::initial_entropy		= "zero"
HydroBase::timelevels		= 1

# =============================================================================
# Templated hydrodynamics code
# =============================================================================
Time::timestep_method		= "given"
Time::timestep			= 0.02

THC_InitialData::id_type		= "static"
## rho = 10^11 g/cc, T = 20 MeV, Ye = 0.3
#THC_InitialData::static_rho		= 1.6191004251588866e-007
#THC_InitialData::static_eps		= 0.71409433190317195
#THC_InitialData::static_ye		= 0.3
## rho = 10^13 g/cc, T = 50 MeV, Ye = 0.1
THC_InitialData::static_rho		= 1.6191004251588866e-5
THC_InitialData::static_eps		= 0.35140529089176736
THC_InitialData::static_ye		= 0.1
## rho = 10^14 g/cc, T = 10.0 MeV, Ye = 0.036
#THC_InitialData::static_rho		= 1.6191004251588865E-004
#THC_InitialData::static_eps		= 2.0656039568649968E-002
#THC_InitialData::static_ye		= 0.036
## rho = 10^11 g/cc, T = 10 MeV, Ye = 0.1
#THC_InitialData::static_rho		= 1.6191004251588866E-007
#THC_InitialData::static_eps		= 6.8107536555442091E-002
#THC_InitialData::static_ye		= 0.1

THC_InitialData::static_velx		= 0.9
#THC_InitialData::static_vely		= 0.3
#THC_InitialData::static_velz		= 0.3

TmunuBase::support_old_CalcTmunu_mechanism	= "no"

EOS_Thermal::evol_eos_name		= "Extable"
EOS_Thermal_Extable::rho_max		= 1.0
EOS_Thermal_Extable::temp_max		= 1000
EOS_Thermal_Extable::extend_ye		= "yes"

EOS_Thermal_Table3d::eos_db_loc		= "/Users/davide/Data/EOS"
EOS_Thermal_Table3d::eos_folder		= "LS220"
EOS_Thermal_Table3d::eos_filename		= "LS_220_hydro_27-Sep-2014.h5"

THC_M1::closure			= "Minerbo"
THC_M1::closure_epsilon		= 1e-8
THC_M1::include_fluxes		= "no"
THC_M1::opacity_corr_fac_max		= 1.0
THC_M1::source_epsabs		= 1e-16
THC_M1::source_epsrel		= 1e-8
THC_M1::source_limiter		= 0.5
THC_M1::source_Ye_min		= 0.035
THC_M1::source_Ye_max		= 0.55
THC_M1::source_thick_limit		= -1.0
THC_M1::source_scat_limit		= -1.0
THC_M1::opacity_tau_trap		= 0.0
THC_M1::opacity_tau_delta		= 1.0

THC_M1::thc_m1_test			= "equil"

THC_Core::eos_type			= "nuclear"
THC_Core::verbose			= "no"

WeakRates::table_filename                                   = "/Users/davide/Data/EOS/LS220/LS_220_weak_27-Sep-2014.h5"

# =============================================================================
# Spacetime evolution
# =============================================================================
ADMBase::evolution_method		= "static"
ADMBase::lapse_evolution_method		= "static"
ADMBase::shift_evolution_method		= "static"
ADMBase::dtlapse_evolution_method		= "static"
ADMBase::dtshift_evolution_method		= "static"

# =============================================================================
# Output
# =============================================================================
IOUtil::out_dir			= $parfile
IOUtil::out_fileinfo		= "none"
IOUtil::strict_io_parameter_check		= "yes"
IOUtil::parfile_write		= "no"

CarpetIOBasic::outinfo_vars		= "
    HydroBase::temperature
    HydroBase::Y_e
"

CarpetIOScalar::outscalar_reductions		= "
    maximum minimum
"
CarpetIOScalar::outscalar_vars		= "
    HydroBase::temperature
    HydroBase::Y_e
    THC_M1::neutrino_fractions
    THC_M1::chi
"

CarpetIOASCII::out1D_vars		= "
    HydroBase::temperature
    HydroBase::Y_e
    HydroBase::vel[0]
    THC_Core::densxp
    THC_Core::densxn
    THC_Core::tau
    THC_M1::neutrino_fractions
    THC_M1::opacs
    THC_M1::rJ
    THC_M1::rnnu
    THC_M1::rN
    THC_M1::rE
    THC_M1::rF
"

CarpetIOBasic::outinfo_every		= 100
CarpetIOScalar::outscalar_every		= 100
CarpetIOASCII::out0D_every		= 100
CarpetIOASCII::output_ghost_points		= "no"
CarpetIOASCII::out1D_every		= 100

# vim: set ft=sh tabstop=20 noexpandtab :
