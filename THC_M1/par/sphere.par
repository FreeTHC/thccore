ActiveThorns = "
ADMBase
ADMCoupling
ADMMacros
AEILocalInterp
Boundary
Carpet
CarpetIOASCII
CarpetIOBasic
CarpetIOHDF5
CarpetIOScalar
CarpetInterp
CarpetLib
CarpetReduce
CarpetSlab
CartGrid3d
CoordBase
CoordGauge
FakeRates
GenericFD
HydroBase
HRSCCore
EOS_Thermal
EOS_Thermal_Idealgas
InitBase
IOUtil
LoopControl
MoL
ReflectionSymmetry
Slab
StaticConformal
SymBase
SystemTopology
THC_Core
THC_M1
THC_Tracer
Time
TmunuBase
"

Cactus::terminate			= "time"
Cactus::cctk_final_time		= 10

# =============================================================================
# Grid
# =============================================================================
Grid::avoid_origin			= "no"
Grid::domain			= "full"
Grid::type			= "coordbase"

CoordBase::xmin			= 0
CoordBase::xmax			= 4
CoordBase::ymin			= 0
CoordBase::ymax			= 4
CoordBase::zmin			= 0
CoordBase::zmax			= 4

CoordBase::spacing			= "numcells"
CoordBase::ncells_x			= 80
CoordBase::ncells_y			= 80
CoordBase::ncells_z			= 80

CoordBase::boundary_size_x_lower		= 2
CoordBase::boundary_size_x_upper		= 2
CoordBase::boundary_shiftout_x_lower		= 0
CoordBase::boundary_shiftout_x_upper		= 1
CoordBase::boundary_staggered_x_lower		= "yes"
CoordBase::boundary_staggered_x_upper		= "yes"

CoordBase::boundary_size_y_lower		= 2
CoordBase::boundary_size_y_upper		= 2
CoordBase::boundary_shiftout_y_lower		= 0
CoordBase::boundary_shiftout_y_upper		= 1
CoordBase::boundary_staggered_y_lower		= "yes"
CoordBase::boundary_staggered_y_upper		= "yes"

CoordBase::boundary_size_z_lower		= 2
CoordBase::boundary_size_z_upper		= 2
CoordBase::boundary_shiftout_z_lower		= 0
CoordBase::boundary_shiftout_z_upper		= 1
CoordBase::boundary_staggered_z_lower		= "yes"
CoordBase::boundary_staggered_z_upper		= "yes"

Driver::ghost_size			= 2
Driver::ghost_size_x		= 2
Driver::ghost_size_y		= 2
Driver::ghost_size_z		= 2

ReflectionSymmetry::reflection_x		= "yes"
ReflectionSymmetry::reflection_y		= "yes"
ReflectionSymmetry::reflection_z		= "yes"

Carpet::domain_from_coordbase		= "yes"

InitBase::initial_data_setup_method		= "init_all_levels"

Carpet::max_refinement_levels		= 1
Carpet::prolongation_order_space		= 3
Carpet::prolongation_order_time		= 2
Carpet::use_buffer_zones		= "yes"
Carpet::enable_all_storage		= "no"

Carpet::grid_coordinates_filename		= "grid.carpet"

# =============================================================================
# Initial data
# =============================================================================
HydroBase::initial_hydro		= "THC_M1"
HydroBase::initial_Y_e		= "one"
HydroBase::initial_temperature		= "zero"
HydroBase::initial_entropy		= "zero"
HydroBase::timelevels		= 1

# =============================================================================
# Templated hydrodynamics code
# =============================================================================
Time::timestep_method		= "given"
Time::timestep			= 0.015

TmunuBase::support_old_CalcTmunu_mechanism	= "no"

EOS_Thermal::evol_eos_name		= "IdealGas"

FakeRates::eta_nue			= 1.0e1
FakeRates::eta_nua			= 1.0e1
FakeRates::eta_nux			= 1.0e1
FakeRates::kappa_scat_nue		= 0.0
FakeRates::kappa_scat_nua		= 0.0
FakeRates::kappa_scat_nux		= 0.0
FakeRates::kappa_abs_nue		= 1.0e1
FakeRates::kappa_abs_nua		= 1.0e1

THC_M1::backreact			= "no"
THC_M1::closure			= "minerbo"
THC_M1::fiducial_velocity		= "fluid"
THC_M1::include_GR_sources		= "no"
THC_M1::minmod_theta		= 1.0
THC_M1::opacity_corr_fac_max		= 1.0
THC_M1::opacity_tau_trap		= -1.0
THC_M1::source_limiter		= -1.0
THC_M1::source_thick_limit		= -1.0
THC_M1::source_epsabs		= 1e-10

THC_M1::thc_m1_test			= "sphere"
THC_M1::set_to_equilibrium		= "no"

THC_Core::verbose			= "no"

# =============================================================================
# Spacetime evolution
# =============================================================================
ADMBase::evolution_method		= "static"
ADMBase::lapse_evolution_method		= "static"
ADMBase::shift_evolution_method		= "static"
ADMBase::dtlapse_evolution_method		= "static"
ADMBase::dtshift_evolution_method		= "static"

# =============================================================================
# Output
# =============================================================================
IOUtil::out_dir			= $parfile
IOUtil::out_fileinfo		= "none"
IOUtil::strict_io_parameter_check		= "yes"
IOUtil::parfile_write		= "no"

CarpetIOBasic::outinfo_vars		= "
    THC_M1::rE[0]
"

CarpetIOScalar::outscalar_reductions		= "
    minimum maximum norm1 norm2
"
CarpetIOScalar::outscalar_vars		= "
    THC_M1::rE[0]
"

CarpetIOASCII::out1d_vars		= "
    HydroBase::rho
    THC_M1::chi[0]
    THC_M1::rN[0]
    THC_M1::rE[0]
    THC_M1::rFx[0]
    THC_M1::rFy[0]
    THC_M1::rFz[0]
"

CarpetIOHDF5::out2d_vars		= "
    HydroBase::rho
    THC_M1::chi[0]
    THC_M1::rN[0]
    THC_M1::rE[0]
    THC_M1::rFx[0]
    THC_M1::rFy[0]
    THC_M1::rFz[0]
"

CarpetIOHDF5::out_vars		= "
    THC_M1::rE[0]
"

CarpetIOBasic::outinfo_every		= 1
CarpetIOScalar::outscalar_every		= 10
CarpetIOASCII::out0D_every		= 10
CarpetIOASCII::out1D_every		= 10
CarpetIOHDF5::out2d_every		= 50
CarpetIOHDF5::out_every		= 50

# vim: set ft=sh tabstop=20 noexpandtab :
