ActiveThorns = "
ADMBase
ADMCoupling
ADMMacros
AEILocalInterp
AHFinderDirect
Boundary
Carpet
CarpetIOASCII
CarpetIOBasic
CarpetIOHDF5
CarpetIOScalar
CarpetInterp
CarpetLib
CarpetMask
CarpetReduce
CarpetSlab
CarpetRegrid2
CartGrid3d
CoordBase
CoordGauge
Dissipation
EOS_Barotropic
EOS_Thermal
EOS_Thermal_Table3d
EOS_Thermal_Extable
Formaline
GenericFD
HydroBase
HRSCCore
InitBase
IOUtil
LoopControl
ML_ADMConstraints
ML_BSSN
ML_BSSN_Helper
MoL
NaNChecker
NewRad
PizzaBase
PizzaIDBase
PizzaNumUtils
PizzaTOV
ID_Switch_EOS
ReflectionSymmetry
Slab
SpaceMask
SphericalSurface
StaticConformal
SymBase
TerminationTrigger
THC_Core
THC_HydroExcision
THC_LeakageBase
THC_LeakageM0
THC_Tracer
Time
TimerReport
TmunuBase
WeakRates
"

Cactus::terminate			= "never"
Cactus::cctk_timer_output                                   = "full"

Formaline::output_source_subdirectory		= "../cactus-source"

TerminationTrigger::max_walltime		= @WALLTIME@
TerminationTrigger::on_remaining_walltime	= 30
TerminationTrigger::create_termination_file	= "yes"
TerminationTrigger::termination_from_file	= "yes"
TerminationTrigger::termination_file		= "../TERMINATE"

TimerReport::output_all_timers_readable		= "yes"
# =============================================================================
# Grid
# =============================================================================
Grid::avoid_origin			= "no"
Grid::domain			= "full"
Grid::type			= "coordbase"

ReflectionSymmetry::reflection_x		= "yes"
ReflectionSymmetry::reflection_y		= "yes"
ReflectionSymmetry::reflection_z		= "yes"
ReflectionSymmetry::avoid_origin_x		= "no"
ReflectionSymmetry::avoid_origin_y		= "no"
ReflectionSymmetry::avoid_origin_z		= "no"

CoordBase::xmin			= 0
CoordBase::xmax			= 80
CoordBase::ymin			= 0
CoordBase::ymax			= 80
CoordBase::zmin			= 0
CoordBase::zmax			= 80

CoordBase::spacing			= "numcells"
CoordBase::ncells_x			= 80
CoordBase::ncells_y			= 80
CoordBase::ncells_z			= 80

CoordBase::boundary_size_x_lower		= 3
CoordBase::boundary_size_x_upper		= 3
CoordBase::boundary_shiftout_x_lower		= 1
CoordBase::boundary_shiftout_x_upper		= 0

CoordBase::boundary_size_y_lower		= 3
CoordBase::boundary_size_y_upper		= 3
CoordBase::boundary_shiftout_y_lower		= 1
CoordBase::boundary_shiftout_y_upper		= 0

CoordBase::boundary_size_z_lower		= 3
CoordBase::boundary_size_z_upper		= 3
CoordBase::boundary_shiftout_z_lower		= 1
CoordBase::boundary_shiftout_z_upper		= 0

Driver::ghost_size			= 3
Driver::ghost_size_x		= 3
Driver::ghost_size_y		= 3
Driver::ghost_size_z		= 3

Carpet::domain_from_coordbase		= "yes"

InitBase::initial_data_setup_method		= "init_all_levels"

Carpet::max_refinement_levels		= 4
Carpet::prolongation_order_space		= 3
Carpet::prolongation_order_time		= 2
Carpet::use_buffer_zones		= "yes"
Carpet::enable_all_storage		= "no"
Carpet::init_fill_timelevels		= "yes"

CarpetRegrid2::num_centres		= 1
CarpetRegrid2::num_levels_1		= 4

CarpetRegrid2::active_1		= "yes"
CarpetRegrid2::radius_1[1]		= 40.0
CarpetRegrid2::radius_1[2]		= 20.0
CarpetRegrid2::radius_1[3]		= 10.0

Carpet::grid_coordinates_filename		= "grid.carpet"

NaNChecker::check_every		= 100
NaNChecker::check_vars		= "HydroBase::w_lorentz"
NaNChecker::action_if_found		= "terminate"

# =============================================================================
# Time integration
# =============================================================================
Carpet::num_integrator_substeps		= 3

MoL::ode_method			= "RK3"
MoL::MoL_Intermediate_Steps		= 3
MoL::MoL_Num_Scratch_Levels		= 0
MoL::verbose			= "register"

HydroBase::timelevels		= 3

Time::timestep_method		= "courant_static"
Time::dtfac			= 0.15

# =============================================================================
# Initial data
# =============================================================================
ADMBase::initial_data		= "PizzaTOV"
ADMBase::initial_lapse		= "PizzaTOV"
ADMBase::initial_shift		= "PizzaTOV"
ADMBase::initial_dtlapse		= "zero"
ADMBase::initial_dtshift		= "zero"
HydroBase::initial_hydro		= "PizzaTOV"
HydroBase::initial_temperature                              = "PizzaTOV"
HydroBase::initial_Y_e                                      = "PizzaTOV"

# File describing a one-parametric EOS in Pizza format. Used only for initial data.
PizzaIDBase::eos_file		= "@EOSDB@/DD2/0.01MeV/eos_dd2_adb.pizza"

ID_Switch_EOS::sync_eps_temp		= "yes"
ID_Switch_EOS::temp_from_eps		= "no"
ID_Switch_EOS::limit_efrac		= "yes"

# TOV central rest mass density in kg/m^3
PizzaTOV::star_crmd			= 1.8e+18
PizzaTOV::kick_amp			= 0.1

# Geometric unit system for initial data, specified by length unit.
# use CACTUS units
PizzaBase::length_unit		= 1476.7161818921163

# =============================================================================
# Templated hydrodynamics code
# =============================================================================
HydroBase::evolution_method		= "THCode"
HydroBase::initial_entropy                                  = "THCode"
HydroBase::entropy_evolution_method		= "THCode"
HydroBase::temperature_evolution_method		= "THCode"
HydroBase::Y_e_evolution_method		= "THCode"

THC_Core::physics			= "GRHD"
THC_Core::eos_type			= "nuclear"

THC_Core::bc_type			= "none"

THC_Core::atmo_rho			= 1e-15
THC_Core::atmo_temperature		= 0.02
#THC_Core::verbose			= "yes"

HRSCCore::scheme			= "FV"
HRSCCore::reconstruction		= "MP5"
HRSCCore::riemann_solver		= "HLLE"

THC_Core::c2a_BH_alp		= 0.15
THC_Core::c2a_rho_strict		= 2.0e-5
THC_Core::c2a_set_to_nan_on_failure		= "no"
THC_Core::c2a_fix_conservatives		= "yes"
THC_Core::c2a_kill_on_failure		= "no"

EOS_Thermal::evol_eos_name		= "Extable"
EOS_Thermal_Extable::rho_max		= 1e10
EOS_Thermal_Extable::temp_max		= 1000
EOS_Thermal_Extable::extend_ye		= "yes"
EOS_Thermal_Table3d::eos_db_loc		= "@EOSDB@"
EOS_Thermal_Table3d::eos_folder		= "DD2"
EOS_Thermal_Table3d::eos_filename		= "DD2_DD2_hydro_30-Mar-2015.h5"

THC_HydroExcision::excision_surface		= 1
THC_LeakageM0::excision		= "yes"
THC_LeakageM0::excision_surface		= 1
THC_LeakageM0::excision_margin		= 1

THC_LeakageBase::neu_abs_type		= "THC_LeakageM0"
THC_LeakageBase::store_free_rates		= "yes"
THC_LeakageBase::store_neu_luminosity		= "yes"
THC_LeakageM0::rmax			= 70
THC_LeakageM0::nrad			= 160
THC_LeakageM0::nray			= 2048

WeakRates::table_filename                                   = "@EOSDB@/DD2/DD2_DD2_weak_30-Mar-2015.h5"
WeakRates::use_rho_max_ext		= "yes"
WeakRates::use_rho_min_ext		= "yes"
WeakRates::use_temp_max_ext		= "yes"
WeakRates::use_temp_min_ext		= "yes"
WeakRates::use_ye_max_ext		= "yes"
WeakRates::use_ye_min_ext		= "yes"

# =============================================================================
# Spacetime evolution
# =============================================================================
TmunuBase::prolongation_type		= "none"
TmunuBase::stress_energy_storage		= "yes"
TmunuBase::stress_energy_at_RHS		= "yes"
TmunuBase::support_old_CalcTmunu_mechanism	= "no"

ADMBase::evolution_method		= "ML_BSSN"
ADMBase::lapse_evolution_method		= "ML_BSSN"
ADMBase::shift_evolution_method		= "ML_BSSN"
ADMBase::dtlapse_evolution_method		= "ML_BSSN"
ADMBase::dtshift_evolution_method		= "ML_BSSN"

ADMBase::lapse_timelevels		= 3
ADMBase::shift_timelevels		= 3
ADMBase::metric_timelevels		= 3

ML_BSSN::timelevels			= 3

ML_BSSN::harmonicN			= 1.0    # 1+log
ML_BSSN::harmonicF			= 2.0    # 1+log
ML_BSSN::ShiftGammaCoeff		= 0.75
ML_BSSN::BetaDriver			= 0.5    # ~ 1/M
ML_BSSN::LapseAdvectionCoeff		= 1.0
ML_BSSN::ShiftAdvectionCoeff		= 1.0

ML_BSSN::MinimumLapse		= 1.0e-8
ML_BSSN::conformalMethod		= 1      # 1 for W
ML_BSSN::dt_lapse_shift_method		= "noLapseShiftAdvection"

ML_BSSN::my_initial_boundary_condition		= "extrapolate-gammas"
ML_BSSN::my_rhs_boundary_condition		= "NewRad"
Boundary::radpower			= 2

ML_BSSN::ML_log_confac_bound		= "none"
ML_BSSN::ML_metric_bound		= "none"
ML_BSSN::ML_Gamma_bound		= "none"
ML_BSSN::ML_trace_curv_bound		= "none"
ML_BSSN::ML_curv_bound		= "none"
ML_BSSN::ML_lapse_bound		= "none"
ML_BSSN::ML_dtlapse_bound		= "none"
ML_BSSN::ML_shift_bound		= "none"
ML_BSSN::ML_dtshift_bound		= "none"

ML_BSSN::fdOrder			= 4
THC_Core::fd_order			= 4

Dissipation::order			= 5
Dissipation::epsdis			= 0.1
Dissipation::vars			= "
ML_BSSN::ML_log_confac
ML_BSSN::ML_metric
ML_BSSN::ML_curv
ML_BSSN::ML_trace_curv
ML_BSSN::ML_Gamma
ML_BSSN::ML_lapse
ML_BSSN::ML_shift
"


# =============================================================================
# Analysis
# =============================================================================

#------SphericalSurface-----------------------
SphericalSurface::nsurfaces		= 2
SphericalSurface::maxntheta		= 55
SphericalSurface::maxnphi		= 96

SphericalSurface::ntheta[0]		= 55
SphericalSurface::nphi[0]		= 96
SphericalSurface::symmetric_z[0]		= "no"
SphericalSurface::nghoststheta[0]		= 2
SphericalSurface::nghostsphi[0]		= 2

SphericalSurface::ntheta[1]		= 55
SphericalSurface::nphi[1]		= 96
SphericalSurface::symmetric_z[1]		= "no"
SphericalSurface::nghoststheta[1]		= 2
SphericalSurface::nghostsphi[1]		= 2

#------AHFinderDirect-----------------------
AHFinderDirect::N_horizons		= 2
AHFinderDirect::find_every		= 64
AHFinderDirect::output_h_every		= 64
AHFinderDirect::max_Newton_iterations__initial	= 50
AHFinderDirect::max_Newton_iterations__subsequent	= 50
AHFinderDirect::max_allowable_Theta_growth_iterations	= 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations	= 10
AHFinderDirect::geometry_interpolator_name	= "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars	= "order=4"
AHFinderDirect::surface_interpolator_name	= "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars	= "order=4"
AHFinderDirect::verbose_level		= "physics details"
AHFinderDirect::move_origins		= "yes"

AHFinderDirect::origin_x[1]		= 0.0
AHFinderDirect::initial_guess__coord_sphere__x_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__y_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__z_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__radius[1]	= 1.8
AHFinderDirect::which_surface_to_store_info[1]	= 0
AHFinderDirect::set_mask_for_individual_horizon[1]	= "no"
AHFinderDirect::reset_horizon_after_not_finding[1]	= "no"
AHFinderDirect::find_after_individual_time[1]	= 8
AHFinderDirect::max_allowable_horizon_radius[1]	= 10.0

AHFinderDirect::origin_x[2]		= 0.0
AHFinderDirect::initial_guess__coord_sphere__x_center[2]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__y_center[2]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__z_center[2]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__radius[2]	= 2.5
AHFinderDirect::which_surface_to_store_info[2]	= 1
AHFinderDirect::set_mask_for_individual_horizon[2]	= "no"
AHFinderDirect::reset_horizon_after_not_finding[2]	= "no"
AHFinderDirect::find_after_individual_time[2]	= 8
AHFinderDirect::max_allowable_horizon_radius[2]	= 10.0

# new parameters suggested by Erik, also for stability at recovery
AHFinderDirect::reshape_while_moving		= "yes"
AHFinderDirect::predict_origin_movement		= "yes"

#--------CarpetMask--------------------------
CarpetMask::excluded_surface[0]		= 0
CarpetMask::excluded_surface_factor[0]		= 1
CarpetMask::excluded_surface[1]		= 1
CarpetMask::excluded_surface_factor[1]		= 1

# =============================================================================
# Checkpoint
# =============================================================================
CarpetIOHDF5::checkpoint		= "yes"
CarpetIOHDF5::use_reflevels_from_checkpoint	= "yes"

IOUtil::checkpoint_on_terminate		= "yes"
IOUtil::checkpoint_every		= 1024
IOUtil::checkpoint_keep		= 1
IOUtil::recover			= "autoprobe"
IOUtil::checkpoint_dir		= "../checkpoint"
IOUtil::recover_dir			= "../checkpoint"

# =============================================================================
# Output
# =============================================================================
IOUtil::out_dir			= "data"
IOUtil::out_fileinfo		= "none"
IOUtil::strict_io_parameter_check		= "yes"
IOUtil::parfile_write		= "no"

CarpetIOBasic::outinfo_vars		= "
THC_LeakageBase::Q_eff_nue
THC_LeakageBase::kappa_0_nue
"

CarpetIOScalar::outscalar_reductions		= "
count minimum maximum average norm1 norm2 norm_inf
"
CarpetIOScalar::outscalar_vars		= "
HydroBase::rho
HydroBase::press
HydroBase::temperature
HydroBase::w_lorentz
HydroBase::Y_e
ML_ADMConstraints::ML_Ham
ML_ADMConstraints::ML_mom
THC_LeakageBase::thc_leakage_abs
THC_LeakageBase::thc_leakage_eff_rates
THC_LeakageBase::thc_leakage_luminosity
THC_LeakageBase::thc_leakage_free_rates
THC_LeakageBase::thc_leakage_optd
"

CarpetIOASCII::out0D_vars		= "
THC_LeakageM0::thc_leakage_M0_is_on
THC_LeakageM0::thc_leakage_M0_flux
"

CarpetIOHDF5::out1D_vars		= "
ADMBase::lapse
ADMBase::shift
ADMBase::metric
HydroBase::rho
HydroBase::press
HydroBase::temperature
HydroBase::vel
HydroBase::Y_e
ML_ADMConstraints::ML_Ham
ML_ADMConstraints::ML_mom
THC_LeakageBase::thc_leakage_abs
THC_LeakageBase::thc_leakage_eff_rates
THC_LeakageBase::thc_leakage_luminosity
THC_LeakageBase::thc_leakage_free_rates
THC_LeakageBase::thc_leakage_optd
THC_LeakageM0::thc_leakage_vars
THC_LeakageM0::thc_leakage_M0_vars
THC_LeakageM0::thc_leakage_M0_evol
THC_LeakageM0::thc_M0_mask
"

CarpetIOHDF5::out2d_vars		= "
HydroBase::rho
HydroBase::press
HydroBase::temperature
HydroBase::Y_e
ML_ADMConstraints::ML_Ham
ML_ADMConstraints::ML_mom
THC_LeakageBase::thc_leakage_abs
THC_LeakageBase::thc_leakage_eff_rates
THC_LeakageBase::thc_leakage_luminosity
THC_LeakageBase::thc_leakage_free_rates
THC_LeakageBase::thc_leakage_optd
"

CarpetIOHDF5::out_vars		= "
HydroBase::rho
HydroBase::press
HydroBase::temperature
HydroBase::Y_e
ML_ADMConstraints::ML_Ham
THC_LeakageBase::thc_leakage_abs
THC_LeakageBase::thc_leakage_eff_rates
THC_LeakageBase::thc_leakage_luminosity
THC_LeakageBase::thc_leakage_free_rates
THC_LeakageBase::thc_leakage_optd
THC_LeakageM0::thc_leakage_vars
THC_LeakageM0::thc_leakage_M0_vars
THC_LeakageM0::thc_leakage_M0_evol
THC_LeakageM0::thc_M0_mask
"

CarpetIOBasic::outinfo_every		= 16
CarpetIOASCII::out0d_every		= 16
CarpetIOScalar::outscalar_every		= 16
CarpetIOHDF5::out1d_every		= 64
CarpetIOHDF5::out2d_every		= 256
CarpetIOHDF5::out_every		= 512

# vim: set ft=sh tabstop=20 noexpandtab :
